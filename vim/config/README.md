# Vim_cpp_IDE

## Installation
### Ubuntu
```
sudo apt-get install exuberant-ctags
sudo apt-get install clang-format
sudo pip install pyclewn
python -c "import clewn; clewn.get_vimball()"
vim -S pyclewn-2.3.vmb

wget http://llvm.org/releases/3.9.0/clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz 
mv clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz ~/VimAsIDE/
cd ~/VimAsIDE
tar xvf clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-16.04.tar.xz 
```


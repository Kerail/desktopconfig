#!/bin/env python

import glob, os
import tempfile
CMakeListsPath = ""
CMakeListsRoot = ""
for root, dirs, files in os.walk(os.getcwd()):
    for file in files:
        if file.endswith("CMakeLists.txt"):
            CMakeListsPath = os.path.join(root, file)
            CMakeListsRoot = root
            break

BuildDirPath = tempfile.mkdtemp(prefix='build_cmake_cpp')

with open('.vimrc','w') as f:
    f.write('let g:cmake_cmake_path="%s"\n' % CMakeListsPath)
    f.write('let g:cmake_build_dir="%s"\n' % BuildDirPath)
    f.write('nnoremap <C-m><C-r> :!cd \'%s\' && cmake \'%s\'<CR>\n' % (BuildDirPath, CMakeListsRoot))
    f.write('nnoremap <C-m><C-m> :!cd \'%s\' && make -j4<CR>\n' % (BuildDirPath))
    f.write('nnoremap <F5> :Pyclewn gdb %s<CR>\n' % (os.path.join(BuildDirPath,'app')))


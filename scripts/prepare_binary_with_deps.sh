#!/bin/bash
SCRIPTS_DIR=`dirname $0`
BINARY_PATH="$1"
OUTPUT_PATH="$2"


if [ ! -d "$OUTPUT_PATH" ]; then
    mkdir -p "$OUTPUT_PATH/deps"
fi

cp "$BINARY_PATH" "$OUTPUT_PATH"
eval "${SCRIPTS_DIR}/copy_deps.sh '$BINARY_PATH' '$OUTPUT_PATH/deps'"
echo '#!/bin/bash' > ${OUTPUT_PATH}/run.sh
echo 'RUN_DIR=`dirname $0`' >> ${OUTPUT_PATH}/run.sh
echo 'export LD_LIBRARY_PATH=${RUN_DIR}/deps:${LD_LIBRARY_PATH}' >> ${OUTPUT_PATH}/run.sh
echo "\${RUN_DIR}/$(basename $BINARY_PATH) \"\$@\"" >> ${OUTPUT_PATH}/run.sh
chmod +x ${OUTPUT_PATH}/run.sh
set nocompatible
filetype off                  " required

set exrc
let g:llvm_path="~/VimAsIDE/clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-14.04/"

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'SirVer/ultisnips'
Plugin 'Rip-Rip/clang_complete'
Plugin 'xaizek/vim-inccomplete' " compeletion for header filenames
Plugin 'majutsushi/tagbar'      " method tree
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'vim-scripts/a.vim' 	" header switch
Plugin 'vim-syntastic/syntastic' "syntastic checking
Plugin 'ctrlpvim/ctrlp.vim'

Bundle 'bling/vim-airline'
Bundle 'jordwalke/flatlandia'
Bundle 'dsolstad/vim-wombat256i'

Plugin 'qpkorr/vim-bufkill'

call vundle#end()            " required
filetype plugin indent on    " required
" """"
" Common
" """"
set backspace=2
set enc=utf-8                                                               " Set UTF-8 encoding
set fenc=utf-8
set termencoding=utf-8
set tabstop=4                                                               " Tab width is 4 spaces
set shiftwidth=4                                                            " Indent also with 4 spaces
set expandtab                                                               " Expand tabs to spaces

set number " Enable line numbering
set numberwidth=3

set cursorline " cursor highlight
set noautoindent


if has("gui_running")                                                       " GUI Vim settings
    set guioptions=agimtTr                                                  " Show 'Menu' + 'Toolbar' + 'Right Scrollbar'
    colorscheme flatlandia                                                   " Set the color scheme
    set background=dark

    set lines=999 columns=999                                               " Run maximized
else                                                                        " Console Vim settings
    set t_Co=256                                                            " Set the color scheme
    colorscheme wombat256i
    if exists("+lines")                                                     " Run maximized
        set lines=50
    endif
    if exists("+columns")
        set columns=100
    endif
endif



" """"
" autocomplete by ctags (includes)
" """""
set nocp
filetype plugin on
map <C-L> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR><CR>

set tags=~/.vim/stdtags,tags,.tags,../tags

autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" """"
" TAGBAR (method tree)
" """"
nnoremap <F8> :TagbarToggle<CR>

" """
" AIRLINE
" """
let g:airline#extensions#tabline#enabled = 1
let g:bufferline_echo = 1
set laststatus=2


" """
" DEBUG
" """
noremap <leader>gb :exe "Cbreak " . expand("%:p") . ":" . line(".")<CR>
noremap <leader>gp :exe "Cprint " . expand("<cword>") <CR>
noremap <leader>gm :exe "Cmapkeys" <CR>
noremap <leader>gu :exe "Cunmapkeys" <CR>

" """"
" FORMAT
" """"
execute "map <C-K> :pyf ".g:llvm_path."share/clang/clang-format.py<CR>"
execute "imap <C-K> <c-o>:pyf ".g:llvm_path."/share/clang/clang-format.py<CR>"
" clang-format -style=llvm -dump-config > .clang-format
"


" """"
" Syntastic
" """"
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:airline#extensions#syntastic#enabled = 1
" let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 '
let g:syntastic_c_include_dirs = ['/usr/include', '/usr/local/include','/usr/include/eigen3']
let g:syntastic_cpp_include_dirs = ['/usr/include', '/usr/local/include','/usr/include/eigen3']

" """"
" CLANG
" """""
let g:clang_library_path="/home/aupixels/VimAsIDE/clang+llvm-3.9.0-x86_64-linux-gnu-ubuntu-14.04/lib/libclang.so"
"set complete-=t                                             " Do not search tag files when auto-completing
"set complete-=i                                             " Do not search include files when auto-completing
set completeopt=menu,menuone                                " Complete options (disable preview scratch window, longest removed to aways show menu)
set pumheight=20                                            " Limit popup menu height
set concealcursor=inv                                       " Conceal in insert (i), normal (n) and visual (v) modes
set conceallevel=2                                          " Hide concealed text completely unless replacement character is defined
let g:clang_use_library = 1                                 " Use libclang directly
let g:clang_complete_auto = 1                               " Run autocompletion immediatelly after ->, ., ::
let g:clang_complete_copen = 1                              " Open quickfix window on error
let g:clang_periodic_quickfix = 0                           " Turn-off periodic updating of quickfix window (g:ClangUpdateQuickFix() does the same)
let g:clang_snippets = 1                                    " Enable function args autocompletion, template parameters, ...
let g:clang_snippets_engine = 'ultisnips'                   " Use UltiSnips engine for function args autocompletion (provides mechanism to jump over to the next argument)

" """"
" Ultisnips
" """"
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsUsePythonVersion = 2
" let g:UltiSnipsSnippetDirectories = ["UltiSnips", "~/.vim/snippets"]



" """"
" Hotkeys
" """"
let mapleader = ","
let maplocalleader = "\\"
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <leader>slv :source .vimrc<cr>

nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel
nnoremap <leader>( viw<esc>a)<esc>hbi(<esc>lel
nnoremap <leader>< viw<esc>a><esc>hbi<<esc>lel

inoremap jk <esc>

nnoremap <F4> :NERDTreeToggle<CR>
nnoremap <C-m><C-t> :pyf ~/.vim/bundle/cmake/config_builder.py<CR>
nnoremap <S-F5> :Cexitclewn<CR>

nnoremap <F12> :bnext<CR>
nnoremap <S-F12> :bprevious<CR>

nnoremap <C-F12> :BD<CR>

" """"
" Abbreviations
" """"

" """"
" Functions
" """"
function! ReformatCPP()
    silent "pyf ".g:llvm_path."/share/clang/clang-format.py<CR>"
    silent "!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q " . bufname("%") 
endfunction

" """"
" Autocommands
" """"
augroup cppFileType
    autocmd!
    autocmd BufWritePre *.cpp,*.hpp,*.h,*.ipp :call ReformatCPP()
augroup END

Configuration for my Linux Setup:

- Ubuntu 16.04
- [i3 window manager](https://i3wm.org/)
- [zsh + oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
- [Terminator](https://gnometerminator.blogspot.com/p/introduction.html)


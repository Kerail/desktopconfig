PWD=`pwd`

###############################################
# Dependencies
###############################################

sudo dpkg -i debs/google-chrome-stable_current_amd64.deb
sudo apt-get install -f -y

sudo dpkg -i debs/teamviewer_13.1.3026_amd64.deb
sudo apt-get install -f -y

sudo apt-get install terminator zsh i3 conky vim screen python git build-essential zsh curl python-pip curl -y
sudo pip install pygments

sudo apt-get install libglib2.0-dev libgdk-pixbuf2.0-dev libxml2-utils -y # theme building
sudo apt-get install rubygems -y
sudo apt-get install ruby ruby-dev -y
sudo gem install sass # theme building

sudo apt-get install lxappearance gtk-chtheme qt4-qtconfig -y # theme switching

sudo apt-get install zathura -y
sudo apt-get install mc -y

sudo apt-get install fonts-inconsolata -y
sudo apt-get install fonts-powerline -y #https://github.com/powerline/fonts
sudo fc-cache -fv

sudo sh -c 'echo "
allow-guest=false
greeter-show-manual-login=true
greeter-hide-users=true
" >> /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf
'

sudo apt-get install thunar thunar-archive-plugin thunar-dropbox-plugin thunar-volman thunar-vcs-plugin -y
sudo apt-get install p7zip-full -y
sudo apt-get install samba -y

sudo apt-get install xserver-xorg-input-synaptics -y

###############################################
# Init
###############################################
git submodule update --init --recursive
# echo "source .bashrc_config" > .basrc 
ln -sf ${PWD}/bashrc_config ~/.bashrc_config
ln -sf ${PWD}/zshrc_config ~/.zshrc_config

###############################################
# ZSH
###############################################

if [ -f ~/.zshrc ]; then
    mv ~/.zshrc ~/.zshrc-pre-install-config
fi
ln -sf ${PWD}/zsh/zshrc ~/.zshrc
ln -sf ${PWD}/zsh/oh-my-zsh ~/.oh-my-zsh
ln -sf ${PWD}/zsh/zsh-syntax-highlighting ~/.zsh-syntax-highlighting

#git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

ln -sf ${PWD}/zsh/dircolors ~/.dircolors


###############################################
# Terminal
###############################################
chsh -s /usr/bin/zsh
mkdir -p ~/.config/terminator
ln -sf ${PWD}/terminator/config ~/.config/terminator/config

###############################################
# Fonts
###############################################
#cd /tmp
#wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
#wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf
#mv PowerlineSymbols.otf ~/.fonts/
#mkdir -p ~/.config/fontconfig/conf.d #if directory doesn't exists

#fc-cache -vf ~/.fonts/

#mv 10-powerline-symbols.conf ~/.config/fontconfig/conf.d/
#cd ${PWD}


###############################################
# i3
###############################################
mkdir -p ~/.i3
mkdir -p ~/.config/i3
ln -sf ${PWD}/i3/config ~/.i3/config
ln -sf ${PWD}/i3/config ~/.config/i3/config

ln -sf ${PWD}/i3/conkyrc ~/.conkyrc
ln -sf ${PWD}/i3/conky-i3bar ~/.conky-i3bar


###############################################
# zathura
###############################################
mkdir -p ~/.config/zathura
ln -sf ${PWD}/zathura/zathurarc ~/.config/zathura/zathurarc

###############################################
# vim
###############################################
ln -sf ${PWD}/vim/config ~/.vim
ln -sf ~/.vim/.vimrc ~/.vimrc 

###############################################
# MC
###############################################
# export MC_SKIN=${PWD}/mc/mc/solarized.ini
ln -sf ${PWD}/mc/mc ~/.mc



###############################################
# GTK
###############################################
ln -sf ${PWD}/themes ~/.themes


sudo add-apt-repository ppa:atareao/telegram
sudo apt update
sudo apt-get install telegram -y
sudo ln -s /opt/telegram/Telegram /usr/local/bin 

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

sudo apt-get update
sudo apt-get install code -y
sudo apt-get install cmake-curses-gui cmake -y
sudo apt-get install autoconf automake libtool curl make g++ unzip -y


sudo ln -s ~/.illia-config/scripts/fixkb.sh /usr/local/bin/

sudo apt-get install xautolock -y
